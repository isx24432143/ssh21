#! /bin/bash

useradd -m -s /bin/bash unix01
useradd -m -s /bin/bash unix02
useradd -m -s /bin/bash unix03
echo -e "unix01\nunix01" | passwd unix01
echo -e "unix02\nunix02" | passwd unix02
echo -e "unix03\nunix03" | passwd unix03

cp /opt/docker/login.defs /etc/login.defs
cp /opt/docker/nscd.conf /etc/nscd.conf
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/pam_ldap.conf /etc/pam_ldap.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
chmod 0600 /etc/nsswitch.conf
/etc/init.d/nscd start
/etc/init.d/ssh start
/usr/sbin/nslcd -d

